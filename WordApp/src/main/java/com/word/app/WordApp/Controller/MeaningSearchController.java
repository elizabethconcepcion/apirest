package com.word.app.WordApp.Controller;

import com.word.app.WordApp.Controller.model.Root;
import com.word.app.WordApp.Controller.model.Definition;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


    public class MeaningSearchController {

        private URL acURL;
        private ObjectMapper objectMapper;


        public MeaningSearchController(String word) throws MalformedURLException, NullPointerException {

            acURL = new URL("https://api.dictionaryapi.dev/api/v2/entries/en/" + word);
            objectMapper = new ObjectMapper();
        }

        public String getMeaning() {
            Root root = null;
            JsonNode rootNodes = null;
            try {
                rootNodes = objectMapper.readTree(acURL).get(0);
                try {
                    root = objectMapper.treeToValue(rootNodes, Root.class);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();

                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return root.getMeanings().get(0).getDefinitions().get(0).getDefinition();

        }

    }



